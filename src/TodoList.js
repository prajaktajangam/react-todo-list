const TodoList = (props) => {
  const TodoItemSelectedToDelete = (id) => {
    console.log(id, " selected");
    props.deleteTodoItemPtr(id);
  };
  return (
    <ul>
      {props.items.map((it) => {
        return (
          <li
            key={it.id}
            onDoubleClick={() => {
              TodoItemSelectedToDelete(it.id);
            }}
          >
            {it.desc}
          </li>
        );
      })}
    </ul>
  );
};
export default TodoList;
