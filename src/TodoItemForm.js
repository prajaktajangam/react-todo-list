import { useState } from "react";

const ToDoItemForm = (props) => {
  const [itemDesc, setItemDesc] = useState("");
  const ItemDescChanged = (event) => {
    setItemDesc(event.target.value);
  };

  const TodoItemAdded = (event) => {
    event.preventDefault();
    console.log("In TodoItemAdded");
    const newTodoItem = {
      id: Math.random(),
      desc: itemDesc,
    };
    props.addTodoItemPtr(newTodoItem);
    setItemDesc("");
  };

  return (
    <form onSubmit={TodoItemAdded}>
      <p>
        Todo Item:
        <input
          type="text"
          name="item-desc"
          value={itemDesc}
          onChange={ItemDescChanged}
        />
        <button type="submit">Add Item</button>
      </p>
    </form>
  );
};

export default ToDoItemForm;
