// db.js
import Dexie from "dexie";

export const db = new Dexie("myTodoList");
db.version(1).stores({
  todoList: "id, itemDesc", // Primary key and indexed props
});
