import { useEffect, useState } from "react";
import { db } from "./db";
import ToDoItemForm from "./TodoItemForm";
import TodoList from "./TodoList";

// const items = [
//   { id: 1, desc: "Wakeup at 6:00am" },
//   { id: 2, desc: "Get milk" },
//   { id: 3, desc: "Make tea" },
// ];
let items = [];
const App = () => {
  const [todoItems, settodoItems] = useState(items);

  const addTodoItem = async (newItem) => {
    console.log("In addTodoItem ", newItem);
    newItem.id = await db.todoList.add(newItem);
    console.log(newItem.id, " added to table myTodoList.todoList");
    settodoItems((prevState) => {
      return [...todoItems, newItem];
    });
  };

  const itemsFromDb = async () => {
    const todoItemsFromDb = await db.table("todoList").toArray();
    console.log({ todoItemsFromDb });
    settodoItems(todoItemsFromDb);
    return todoItemsFromDb;
  };

  const deleteTodoItem = async (id) => {
    await db.table("todoList").delete(id);
    itemsFromDb();
  };

  useEffect(() => {
    itemsFromDb();
  }, []);

  return (
    <>
      <ToDoItemForm addTodoItemPtr={addTodoItem}></ToDoItemForm>
      <TodoList items={todoItems} deleteTodoItemPtr={deleteTodoItem}></TodoList>
    </>
  );
};

export default App;
